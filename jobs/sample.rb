current_valuation = 0
current_karma = 0
current_number_users = 0

require 'open-uri'
require 'rubygems'
require 'json'

users_string = open('https://api-test.projectcamp.us/courses/course-1/users').read
users_parsed = JSON.parse(users_string)
current_number_users = users_parsed.length


SCHEDULER.every '2s' do
  last_valuation = current_valuation
  last_karma     = current_karma
  current_valuation = rand(100)
  current_karma     = rand(200000)
  last_number_users = current_number_users
  #current_number_users = 10
 # current_number_users = users_parsed.length

  send_event('valuation', { current: current_valuation, last: last_valuation })
  send_event('karma', { current: current_karma, last: last_karma })
  send_event('synergy',   { value: rand(100) })
  send_event('users',{current: current_number_users, last: last_number_users})
end